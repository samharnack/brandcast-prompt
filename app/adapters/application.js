import DS from 'ember-data';

export default DS.RESTAdapter.extend({
  findRecord (store, type, id, snapshot) {
    return this.store.findAll(type.modelName).then(function (records) {
      return records.findBy('id', id);
    });
  },

  findAll(store, type, sinceToken) {
    return Ember.$.get('data.json');
  }
});
