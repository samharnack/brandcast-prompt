import Ember from 'ember';

export default Ember.Controller.extend({
  users: Ember.computed.alias('content')
});
