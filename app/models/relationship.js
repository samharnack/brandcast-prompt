import DS from 'ember-data';

export default DS.Model.extend({
  parent: DS.belongsTo('user'),
  child: DS.belongsTo('user'),
  atributes: DS.attr('object')
});
