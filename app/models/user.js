import Ember from 'ember';
import DS from 'ember-data';

export default DS.Model.extend({
  name: Ember.computed.alias('attributes.name'),

  attributes: DS.attr('object'),

  spouseRelationships: DS.hasMany('spouse-relation', {
    polymorphic: true,
    inverse: null
  }),

  children: DS.hasMany('child-relation', {
    polymorphic: true,
    inverse: 'parent'
  }),

  parents: DS.hasMany('child-relation', {
    polymorphic: true,
    inverse: 'child'
  }),

  allSiblings: Ember.computed('parents.@each.children', function () {
    return this.get('parents').reduce(function (siblings, parent) {
      return siblings.concat(parent.get('parent.children').toArray());
    }, []);
  }),

  siblingsWithoutSelf: Ember.computed.filter('allSiblings', function (sibling) {
    return sibling.get('child.id') != this.get('id');
  }),

  siblings: Ember.computed.uniqBy('siblingsWithoutSelf', 'child.id'),

  spouse: Ember.computed.map('spouseRelationships', function (relationship) {
    if (relationship.get('parent.id') === this.get('id')) {
      return relationship.get('child');
    }

    return relationship.get('parent');
  })
});
