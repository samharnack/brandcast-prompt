# Relationships

- id
- type [parent, spouse, pet]
  - NOTES: parent/child relation ships are normalized so the parent is always stored as the parent_user and the child is always as the user
- parent_user_id
- user_id
- attributes {}


# User

- id
- type [human, metahuman, mutant, dog, cat]
- attributes {}
- name
- relationships [where id in (parent_user_id, user_id)]